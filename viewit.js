// Script used to inject the viewit app the into shapespark body-end.html
function useLocalStartupScript() {
  const urlParams = new URLSearchParams(window.location.search);
  const startenv = urlParams.get("startenv");
  let useLocalStartupScript = false;
  if (startenv !== undefined && startenv === "loc") {
    useLocalStartupScript = true;
  }
  return useLocalStartupScript;
}

function loadScript(scriptUrl) {
  return new Promise((resolve, reject) => {
    let scriptToLoad = document.createElement("script");
    scriptToLoad.onload = () => {
      resolve();
    };
    scriptToLoad.setAttribute("src", scriptUrl);
    scriptToLoad.setAttribute("type", "text/javascript");
    //scriptToLoad.setAttribute('crossorigin', 'anonymous');
    document.body.appendChild(scriptToLoad);
  });
}

if (useLocalStartupScript()) {
  // Start with locally hosted script
  loadScript("http://127.0.0.1:3000/public/viewitInterfaceScript.js");
} else {
  function getViewitScriptUrl() {
    let startScriptName = "index.bundle.js";
    let scriptUri = {
      loc: "http://localhost:3000/",
      dev: "https://prompto-viewit-dev.appspot.com/",
      prod: "https://prompto-viewit.appspot.com/",
    };

    const urlParams = new URLSearchParams(window.location.search);
    const env = urlParams.get("env");
    let scriptUriToLoad = scriptUri.prod;
    if (env !== undefined && scriptUri[env] !== undefined) {
      scriptUriToLoad = scriptUri[env];
    }
    return scriptUriToLoad + startScriptName;
  }

  let appRootNode = document.createElement("div");
  appRootNode.setAttribute("class", "fullscreen");
  appRootNode.setAttribute("id", "root");
  document.body.appendChild(appRootNode);

  loadScript("https://unpkg.com/react@16/umd/react.development.js")
    .then(() => {
      loadScript("https://unpkg.com/react-dom@16/umd/react-dom.development.js");
    })
    .then(() => {
      loadScript(getViewitScriptUrl());
    });

  let styleNode = document.createElement("style");
  styleNode.innerHTML = `.fullscreen {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    margin: 0;
    pointer-events: none;
  }`;
  document.body.appendChild(styleNode);
}
